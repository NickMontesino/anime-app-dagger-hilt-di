package com.alecbrando.homeworkrecyclerview.model.models

data class Medium(
    val height: Int,
    val width: Int
)